#!/bin/bash

# This is the config for Raspberry Pi 4
# for older version you should have a look at 
# https://github.com/adrianmihalko/raspberrypiwireguard

set -e

# Start bind9
/usr/sbin/named

# Install Wireguard. This has to be done dynamically since the kernel module depends on the host kernel version.
# https://www.wireguard.com/install/#debian-module-tools & https://github.com/activeeos/wireguard-docker
echo "deb http://raspbian.raspberrypi.org/raspbian/ buster main contrib non-free rpi" > /etc/apt/sources.list.d/rpi.list
echo "deb http://archive.raspberrypi.org/debian/ buster main" >> /etc/apt/sources.list.d/rpi.list
echo "deb http://deb.debian.org/debian/ unstable main" > /etc/apt/sources.list.d/unstable.list
printf 'Package: *\nPin: release a=unstable\nPin-Priority: 90\n' > /etc/apt/preferences.d/limit-unstable
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8B48AD6246925553 7638D0442B90D010 04EE7237B7D453EC 9165938D90FDDD2E 82B129927FA3303E
apt-get update
apt-get autoremove -y
apt-get upgrade -y
apt-get install -y raspberrypi-kernel-headers dirmngr
apt-get install -y wireguard
apt-get clean
depmod
modprobe wireguard

chmod 600 /etc/wireguard/*
cd /etc/wireguard/

echo "$(date): Starting Wireguard"
wg-quick up wg0
ip address add dev wg0 10.200.200.254/24

# port forwarding example
# Postfix (SMTP). 
# 192.168.200.1 is the SMTP server (running on the Docker host)
iptables -A PREROUTING -t nat --dst 10.200.200.254 -p tcp --dport 25 -j DNAT --to 192.168.200.1:25

trap finish SIGTERM SIGINT SIGQUIT

sleep infinity &
wait $!

# EOF