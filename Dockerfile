FROM debian:stable-slim

MAINTAINER Yeri Tiete <yeri@tiete.be>

ARG HOSTNAME

ENV DEBIAN_FRONTEND="noninteractive"

COPY run.sh /run.sh

RUN mkdir /etc/sysctl.d/ ; echo net.ipv4.ip_forward=1 > /etc/sysctl.d/99-sysctl.conf ; \
	echo net.ipv6.conf.all.forwarding=1 >> /etc/sysctl.d/99-sysctl.conf ; \
	echo net.ipv4.conf.all.proxy_arp=1 >> /etc/sysctl.d/99-sysctl.conf ; \
	mkdir /etc/wireguard

RUN apt-get update && \
	apt-get upgrade -y && \
	apt-get install -y unattended-upgrades apt-listchanges iptables iproute2 apt-utils bind9 dnsutils gnupg2 bc

WORKDIR /etc/wireguard/

COPY files/wg*.conf /etc/wireguard/

COPY files/named.conf.options /etc/bind/named.conf.options

COPY files/02periodic /etc/apt/apt.conf.d/02periodic

EXPOSE 7999-8000:7999-8000/udp

ENTRYPOINT ["/run.sh"]

CMD []