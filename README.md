# Box
## Info
All the information can be found in this [post](https://yeri.be/running-wireguard-in-a-docker-container-(amd64)) and this [post](https://yeri.be/running-wireguard-in-a-docker-container-rpi).

## Maintained
This repo will rarely or never be updated but acts as a proof of concept

## Contact
Reach out to [Yeri](https://yeri.be) at tiete dot be.