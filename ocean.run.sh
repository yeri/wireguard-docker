#!/bin/bash

set -e

# Start bind9
/usr/sbin/named

# Install Wireguard. This has to be done dynamically since the kernel module depends on the host kernel version.
# https://www.wireguard.com/install/#debian-module-tools & https://github.com/activeeos/wireguard-docker
echo "deb http://deb.debian.org/debian buster-backports main" > /etc/apt/sources.list.d/backports.list

apt-get update
apt-get autoremove -y
apt-get upgrade -y
apt-get install -y linux-headers-$(uname -r)
apt-get install -y wireguard
apt-get clean
depmod
modprobe wireguard

chmod 600 /etc/wireguard/*
cd /etc/wireguard/

echo "$(date): Starting Wireguard"
wg-quick up wg0
ip address add dev wg0 10.200.200.1/24

# Currently not in use
#wg-quick up wg1
#ip address add dev wg0 10.222.111.1/24 # be aware that the ip route add below will be available to this network

# port forwarding example
# Postfix (SMTP)
#iptables -A PREROUTING -t nat --dst 10.200.200.1 -p tcp --dport 25 -j DNAT --to 192.168.200.1:25

trap finish SIGTERM SIGINT SIGQUIT

sleep infinity &
wait $!

# EOF