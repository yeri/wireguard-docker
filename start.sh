#!/bin/bash

# Running the script will update the repo
git pull

# I have one repo for several hosts. This should get you started to customise it per host
# In the example below I only have one WG file (wg0), but you can create multiple files as you please
 if [ "$HOSTNAME" = ocean ]; then

	DNS=10.13.37.1
	
	cp files/ocean.wg0.conf files/wg0.conf
	cp ocean.run.sh run.sh

elif [[ "$HOSTNAME" = liana ]]; then

	DNS=10.19.88.1
	
	cp files/liana.wg0.conf files/wg0.conf
	cp liana.run.sh run.sh

else
	echo "Unknown host"
	exit 1;
fi

TIMEZONE=`cat /etc/timezone`

cp files/named.conf.options.default files/named.conf.options
# Set the DNS forwarder (as defined above) per container
sed -i "s/CHANGETHEDNSSERVER/$DNS/g" files/named.conf.options

docker stop wireguard
docker rm wireguard
docker build -t wireguard .
# This requires --privileged and --cap-add sys_module to work. 
# Be sure to install the kernel headers for your kernel
# I am also running my own network (--network 0x04), so be sure to create this one with docker network create
# And lastly, I assign a static IP address to the container. 
# I am also exposing UDP port 7999 and 8000 -- 7999 is used by wg0 and 8000 is used by wg1 (which is not included in this example)
docker run --privileged --sysctl net.ipv4.ip_forward=1 --sysctl net.ipv6.conf.all.forwarding=1 --cap-add net_admin --cap-add sys_module --dns $DNS -e TZ=$TIMEZONE -v /lib/modules:/lib/modules -d --network 0x04 --ip 192.168.200.100 -p 7999-8000:7999-8000/udp --restart always --hostname wireguard --name wireguard wireguard

# clean up 
rm -f run.sh files/wg0.conf files/wg1.conf files/named.conf.options